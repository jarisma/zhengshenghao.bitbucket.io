function addFlag(sandbox) {
    let tagPose = {
        lng: 156.6,
        lat: -45.5,
        alt: 300000,
        north: 270,
        tilt: 0
    };

    let tag = new altizure.TagMarker({
        isSprite: false,
        depthTest: true,
        imgUrl: '../public/image/flag.png',
        position: {
            lng: tagPose.lng,
            lat: tagPose.lat,
            alt: tagPose.alt / 2
        },
        sandbox: sandbox,
        scale: 100000
    });
    // tag.tagQuaternion = {
    //     x: Math.PI / 3,
    //     y: 0.5,
    //     z: -0.5,
    //     order: 'XYZ',
    //     isEuler: true
    // };
    tag.tagQuaternion = {
        x: Math.PI,
        y: Math.PI / 2,
        z: -Math.PI / 2,
        order: "XYZ",
        isEuler: true
    };
}

function landing(sandbox) {
    return new Promise(function(resolve, reject) {
        //  let pose = {
        //      lng: 177.6000000,
        //      lat: -45.50000,
        //      alt: 1000000,
        //      north: 270,
        //      tilt: 0
        //  };
        let pose = { lng: 157, lat: -45, alt: 2350703, north: -90, tilt: 0 }

        // let button = document.createElement('button');
        // button.style.position = 'fixed';
        // button.style.zIndex = 100;
        // button.style.left = '10px';
        // button.style.top = '10px';
        // button.style.border = '1px solid #fff';
        // button.style.color = '#fff';
        // button.style.background = 'rgb(255, 132, 132)';
        // button.innerHTML = '减速';


        let g = 9.8;
        let endAlt = 500000;
        let currentAlt = pose.alt;
        let time = 10000;
        let speed = (currentAlt - endAlt) / time;
        let successSpeed = 150;
        let p = sandbox.camera.flyTo({
            lng: pose.lng,
            lat: pose.lat,
            alt: pose.alt,
            north: pose.north,
            tilt: pose.tilt
        });

        let setting = new function() {
            this.message = '控制面板';
            this.speed = speed;
            this.distance = currentAlt - endAlt;
            this.slow = function() {
                console.log(speed)

                if (speed < 10) {
                    console.log('不可以再减速了！');
                    return;
                }
                currentAlt = sandbox.camera.pose.alt;
                if (currentAlt - endAlt <= 0) {
                    console.log('已到达终点！');
                    return;
                }
                setting.speed = speed -= 10;
                time = (currentAlt - endAlt) / speed;
                sandbox.camera.flyTo({
                    lng: pose.lng,
                    lat: pose.lat,
                    alt: endAlt,
                    north: pose.north,
                    tilt: pose.tilt
                }, time);
                setting.distance = sandbox.camera.pose.alt - endAlt;
                console.log('click:' + currentAlt, speed, time);
            }
        };

        p.then((res) => {
            // document.getElementsByTagName('body')[0].appendChild(button);

            // let gui = new dat.GUI();
            // gui.add(setting, 'message');
            // gui.add(setting, 'speed').listen();
            // gui.add(setting, 'distance').listen();
            // gui.add(setting, 'slow');
            setting.distance = sandbox.camera.pose.alt - endAlt;
            sandbox.camera.flyTo({
                lng: pose.lng,
                lat: pose.lat,
                alt: endAlt,
                north: pose.north,
                tilt: pose.tilt
            }, time);
            let timer = setInterval(function() {
                time -= 1000;
                if (time < 0 || (sandbox.camera.pose.alt - endAlt) <= 0) {
                    if ((sandbox.camera.pose.alt - endAlt) <= 0) {
                        if (speed <= successSpeed) {
                            console.log('congratulate!');
                            let res = true;
                            resolve(res);
                        } else {
                            console.log('fail!');
                            let res = false;
                            resolve(res);
                        }
                    }
                    clearInterval(timer);
                    timer = null;
                    return;
                }
                sandbox.camera.flyTo({
                    lng: pose.lng,
                    lat: pose.lat,
                    alt: endAlt,
                    north: pose.north,
                    tilt: pose.tilt
                }, time).then(function(res) {
                    console.log(res, time, speed, sandbox.camera.pose.alt);
                });
                setting.distance = sandbox.camera.pose.alt - endAlt;
                setting.speed = speed += g;
            }, 1000);
        });
        document.querySelector('.slow-container').onclick = function(e) {
            console.log(speed);

            if (speed < 10) {
                console.log('不可以再减速了！');
                return;
            }
            currentAlt = sandbox.camera.pose.alt;
            if (currentAlt - endAlt <= 0) {
                console.log('已到达终点！');
                return;
            }
            setting.speed = speed -= 10;
            time = (currentAlt - endAlt) / speed;
            sandbox.camera.flyTo({
                lng: pose.lng,
                lat: pose.lat,
                alt: endAlt,
                north: pose.north,
                tilt: pose.tilt
            }, time);
            setting.distance = sandbox.camera.pose.alt - endAlt;
            console.log('click:' + currentAlt, speed, time);
        }
    });
}